﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityZaichegTools_All;

public class DataContainer : Singleton<DataContainer>
{
	public TextAsset Players_TextAsset;
	public TextAsset Maps_TextAsset;
	public TextAsset Champions_TextAsset;

	public List<string> Players = new List<string>();
	public List<string> Maps = new List<string>();
	public List<string> Champions = new List<string>();

	public void UnityAwakeEvent()
	{
		Players = new List<string>(Players_TextAsset.text.Split(new string[] { "\r\n" }, System.StringSplitOptions.RemoveEmptyEntries));
		Maps = new List<string>(Maps_TextAsset.text.Split(new string[] { "\r\n" }, System.StringSplitOptions.RemoveEmptyEntries));
		Champions = new List<string>(Champions_TextAsset.text.Split(new string[] { "\r\n" }, System.StringSplitOptions.RemoveEmptyEntries));
	}
}