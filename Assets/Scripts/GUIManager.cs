﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityZaichegTools_All;
using System;

public class GUIManager : Singleton<GUIManager>
{
	public TMP_Text Info_Text;
	public Button ReadLeagueFile_Button;
	public Button SaveLeagueFile_Button;

	public TMP_Dropdown Stage_Dropdown;
	public TMP_Dropdown Week_Dropdown;

	public TMP_Dropdown BluePlayerName_Dropdown;
	public Toggle BluePlayerCoin_Toggle;
	public TMP_Dropdown RedPlayerName_Dropdown;
	public Toggle RedPlayerCoin_Toggle;

	public TMP_InputField VideoLink_InputField;

	public TMP_Dropdown BannedMap_01_MapName_Dropdown;
	public TMP_Dropdown BannedMap_02_MapName_Dropdown;

	public TMP_Dropdown PickedMap_03_MapName_Dropdown;
	public TMP_Dropdown PickedMap_03_BluePlayerPick_Dropdown;
	public TMP_Dropdown PickedMap_03_BluePlayerScore_Dropdown;
	public TMP_Dropdown PickedMap_03_BluePlayerBan_Dropdown;
	public TMP_Dropdown PickedMap_03_RedPlayerPick_Dropdown;
	public TMP_Dropdown PickedMap_03_RedPlayerScore_Dropdown;
	public TMP_Dropdown PickedMap_03_RedPlayerBan_Dropdown;

	public TMP_Dropdown PickedMap_04_MapName_Dropdown;
	public TMP_Dropdown PickedMap_04_BluePlayerPick_Dropdown;
	public TMP_Dropdown PickedMap_04_BluePlayerScore_Dropdown;
	public TMP_Dropdown PickedMap_04_BluePlayerBan_Dropdown;
	public TMP_Dropdown PickedMap_04_RedPlayerPick_Dropdown;
	public TMP_Dropdown PickedMap_04_RedPlayerScore_Dropdown;
	public TMP_Dropdown PickedMap_04_RedPlayerBan_Dropdown;

	public TMP_Dropdown BannedMap_05_MapName_Dropdown;
	public TMP_Dropdown BannedMap_06_MapName_Dropdown;

	public TMP_Dropdown PickedMap_07_MapName_Dropdown;
	public TMP_Dropdown PickedMap_07_BluePlayerPick_Dropdown;
	public TMP_Dropdown PickedMap_07_BluePlayerScore_Dropdown;
	public TMP_Dropdown PickedMap_07_BluePlayerBan_Dropdown;
	public TMP_Dropdown PickedMap_07_RedPlayerPick_Dropdown;
	public TMP_Dropdown PickedMap_07_RedPlayerScore_Dropdown;
	public TMP_Dropdown PickedMap_07_RedPlayerBan_Dropdown;

	public Button AddMatch_Button;
	public Button CreateReport_01_Button;
	public Button CreateReport_02_Button;
	public Button CreateReport_03_Button;
	public Button CreateReport_04_Button;
	public Button CreateReport_05_Button;
	public Button CreateReport_06_Button;

	public void UnityOnEnableEvent()
	{
		FillValues();

		ReadLeagueFile_Button.onClick.AddListener(ReadStageLeague_Button_Button_onClick_Event);
		SaveLeagueFile_Button.onClick.AddListener(SaveLeagueFile_Button_Button_onClick_Event);
		AddMatch_Button.onClick.AddListener(AddMatch_Button_onClick_Event);
		CreateReport_01_Button.onClick.AddListener(CreateReport_01_Button_onClick_Event);
		CreateReport_02_Button.onClick.AddListener(CreateReport_02_Button_onClick_Event);
		CreateReport_03_Button.onClick.AddListener(CreateReport_03_Button_onClick_Event);
		CreateReport_04_Button.onClick.AddListener(CreateReport_04_Button_onClick_Event);
		CreateReport_05_Button.onClick.AddListener(CreateReport_05_Button_onClick_Event);
		CreateReport_06_Button.onClick.AddListener(CreateReport_06_Button_onClick_Event);

		BluePlayerCoin_Toggle.onValueChanged.AddListener(BluePlayerCoin_Toggle_onValueChanged_Event);
		RedPlayerCoin_Toggle.onValueChanged.AddListener(RedPlayerCoin_Toggle_onValueChanged_Event);
	}

	public void FillValues()
	{
		BluePlayerName_Dropdown.value = 0;
		BluePlayerName_Dropdown.ClearOptions();
		BluePlayerName_Dropdown.AddOptions(DataContainer.Instance.Players);

		RedPlayerName_Dropdown.value = 0;
		RedPlayerName_Dropdown.ClearOptions();
		RedPlayerName_Dropdown.AddOptions(DataContainer.Instance.Players);

		var mapsDropdowns = new List<TMP_Dropdown>()
		{
			BannedMap_01_MapName_Dropdown, BannedMap_02_MapName_Dropdown, PickedMap_03_MapName_Dropdown,
			PickedMap_04_MapName_Dropdown, BannedMap_05_MapName_Dropdown, BannedMap_06_MapName_Dropdown, PickedMap_07_MapName_Dropdown
		};

		var championsDropdowns = new List<TMP_Dropdown>()
		{
			PickedMap_03_BluePlayerPick_Dropdown, PickedMap_03_RedPlayerPick_Dropdown, PickedMap_03_BluePlayerBan_Dropdown, PickedMap_03_RedPlayerBan_Dropdown,
			PickedMap_04_BluePlayerPick_Dropdown, PickedMap_04_RedPlayerPick_Dropdown, PickedMap_04_BluePlayerBan_Dropdown, PickedMap_04_RedPlayerBan_Dropdown,
			PickedMap_07_BluePlayerPick_Dropdown, PickedMap_07_RedPlayerPick_Dropdown, PickedMap_07_BluePlayerBan_Dropdown, PickedMap_07_RedPlayerBan_Dropdown
		};

		var scoredDropdowns = new List<TMP_Dropdown>()
		{
			PickedMap_03_BluePlayerScore_Dropdown, PickedMap_03_RedPlayerScore_Dropdown,
			PickedMap_04_BluePlayerScore_Dropdown, PickedMap_04_RedPlayerScore_Dropdown,
			PickedMap_07_BluePlayerScore_Dropdown, PickedMap_07_RedPlayerScore_Dropdown,
		};

		foreach (var dropdown in mapsDropdowns)
		{
			dropdown.value = 0;
			dropdown.ClearOptions();
			dropdown.AddOptions(DataContainer.Instance.Maps);
		}

		foreach (var dropdown in championsDropdowns)
		{
			dropdown.value = 0;
			dropdown.ClearOptions();
			dropdown.AddOptions(DataContainer.Instance.Champions);
		}

		foreach (var dropdown in scoredDropdowns)
		{
			dropdown.value = 0;
			dropdown.ClearOptions();
			for (var i = -1; i < 40; i++)
				dropdown.AddOptions(new List<string>() { i.ToString() });
		}

		BluePlayerCoin_Toggle.isOn = false;
		RedPlayerCoin_Toggle.isOn = false;
		VideoLink_InputField.text = "";
	}

	private void BluePlayerCoin_Toggle_onValueChanged_Event(bool _value)
	{
		if (_value)
		{
			PickedMap_03_BluePlayerBan_Dropdown.gameObject.SetActive(true);
			PickedMap_04_BluePlayerBan_Dropdown.gameObject.SetActive(false);
			PickedMap_07_BluePlayerBan_Dropdown.gameObject.SetActive(true);

			PickedMap_03_RedPlayerBan_Dropdown.gameObject.SetActive(false);
			PickedMap_04_RedPlayerBan_Dropdown.gameObject.SetActive(true);
			PickedMap_07_RedPlayerBan_Dropdown.gameObject.SetActive(false);
		}
		else
		{
			PickedMap_03_BluePlayerBan_Dropdown.gameObject.SetActive(true);
			PickedMap_04_BluePlayerBan_Dropdown.gameObject.SetActive(true);
			PickedMap_07_BluePlayerBan_Dropdown.gameObject.SetActive(true);

			PickedMap_03_RedPlayerBan_Dropdown.gameObject.SetActive(true);
			PickedMap_04_RedPlayerBan_Dropdown.gameObject.SetActive(true);
			PickedMap_07_RedPlayerBan_Dropdown.gameObject.SetActive(true);
		}
	}
	private void RedPlayerCoin_Toggle_onValueChanged_Event(bool _value)
	{
		if (_value)
		{
			PickedMap_03_BluePlayerBan_Dropdown.gameObject.SetActive(false);
			PickedMap_04_BluePlayerBan_Dropdown.gameObject.SetActive(true);
			PickedMap_07_BluePlayerBan_Dropdown.gameObject.SetActive(false);

			PickedMap_03_RedPlayerBan_Dropdown.gameObject.SetActive(true);
			PickedMap_04_RedPlayerBan_Dropdown.gameObject.SetActive(false);
			PickedMap_07_RedPlayerBan_Dropdown.gameObject.SetActive(true);
		}
		else
		{
			PickedMap_03_BluePlayerBan_Dropdown.gameObject.SetActive(true);
			PickedMap_04_BluePlayerBan_Dropdown.gameObject.SetActive(true);
			PickedMap_07_BluePlayerBan_Dropdown.gameObject.SetActive(true);

			PickedMap_03_RedPlayerBan_Dropdown.gameObject.SetActive(true);
			PickedMap_04_RedPlayerBan_Dropdown.gameObject.SetActive(true);
			PickedMap_07_RedPlayerBan_Dropdown.gameObject.SetActive(true);
		}
	}

	private void ReadStageLeague_Button_Button_onClick_Event()
	{
		General.Instance.ReadStageFile();
	}
	private void SaveLeagueFile_Button_Button_onClick_Event()
	{
		General.Instance.SaveLeagueFile();
	}
	private void AddMatch_Button_onClick_Event()
	{
		General.Instance.AddMatchToLeagueFile();
	}
	private void CreateReport_01_Button_onClick_Event()
	{
		General.Instance.CreateReport_01();
	}
	private void CreateReport_02_Button_onClick_Event()
	{
		General.Instance.CreateReport_02();
	}
	private void CreateReport_03_Button_onClick_Event()
	{
		General.Instance.CreateReport_03();
	}
	private void CreateReport_04_Button_onClick_Event()
	{
		General.Instance.CreateReport_04();
	}
	private void CreateReport_05_Button_onClick_Event()
	{
		General.Instance.CreateReport_05();
	}
	private void CreateReport_06_Button_onClick_Event()
	{
		General.Instance.CreateReport_06();
	}

	public void AddDataFromUI(League _league)
	{
		//=== Заполнение данных матча

		var match = new Match();

		var bluePlayer = new Player();
		bluePlayer.PlayerName = GetDropdownText(BluePlayerName_Dropdown);
		bluePlayer.IsCoin = BluePlayerCoin_Toggle.isOn;
		match.BluePlayer = bluePlayer;

		var redPlayer = new Player();
		redPlayer.PlayerName = GetDropdownText(RedPlayerName_Dropdown);
		redPlayer.IsCoin = RedPlayerCoin_Toggle.isOn;
		match.RedPlayer = redPlayer;

		match.VideoLink = VideoLink_InputField.text;

		var isUnknownCoin = match.BluePlayer.IsCoin == false && match.RedPlayer.IsCoin == false;

		//=== Заполнение данных карт

		match.BannedMap_01 = new BannedMap();
		match.BannedMap_01.MapName = GetDropdownText(BannedMap_01_MapName_Dropdown);

		match.BannedMap_02 = new BannedMap();
		match.BannedMap_02.MapName = GetDropdownText(BannedMap_02_MapName_Dropdown);

		match.PickedMap_03 = new PickedMap();
		match.PickedMap_03.MapName = GetDropdownText(PickedMap_03_MapName_Dropdown);
		match.PickedMap_03.BluePlayer.PlayerScore = GetDropdownTextAsInt(PickedMap_03_BluePlayerScore_Dropdown);
		match.PickedMap_03.RedPlayer.PlayerScore = GetDropdownTextAsInt(PickedMap_03_RedPlayerScore_Dropdown);
		if (match.BluePlayer.IsCoin || isUnknownCoin)
			match.PickedMap_03.BluePlayer.BannedChampionName = GetDropdownText(PickedMap_03_BluePlayerBan_Dropdown);
		if (match.RedPlayer.IsCoin || isUnknownCoin)
			match.PickedMap_03.RedPlayer.BannedChampionName = GetDropdownText(PickedMap_03_RedPlayerBan_Dropdown);
		match.PickedMap_03.BluePlayer.PickedChampionName = GetDropdownText(PickedMap_03_BluePlayerPick_Dropdown);
		match.PickedMap_03.RedPlayer.PickedChampionName = GetDropdownText(PickedMap_03_RedPlayerPick_Dropdown);

		match.PickedMap_04 = new PickedMap();
		match.PickedMap_04.MapName = GetDropdownText(PickedMap_04_MapName_Dropdown);
		match.PickedMap_04.BluePlayer.PlayerScore = GetDropdownTextAsInt(PickedMap_04_BluePlayerScore_Dropdown);
		match.PickedMap_04.RedPlayer.PlayerScore = GetDropdownTextAsInt(PickedMap_04_RedPlayerScore_Dropdown);
		if (match.RedPlayer.IsCoin || isUnknownCoin)
			match.PickedMap_04.BluePlayer.BannedChampionName = GetDropdownText(PickedMap_04_BluePlayerBan_Dropdown);
		if (match.BluePlayer.IsCoin || isUnknownCoin)
			match.PickedMap_04.RedPlayer.BannedChampionName = GetDropdownText(PickedMap_04_RedPlayerBan_Dropdown);
		match.PickedMap_04.BluePlayer.PickedChampionName = GetDropdownText(PickedMap_04_BluePlayerPick_Dropdown);
		match.PickedMap_04.RedPlayer.PickedChampionName = GetDropdownText(PickedMap_04_RedPlayerPick_Dropdown);

		match.BannedMap_05 = new BannedMap();
		match.BannedMap_05.MapName = GetDropdownText(BannedMap_05_MapName_Dropdown);

		match.BannedMap_06 = new BannedMap();
		match.BannedMap_06.MapName = GetDropdownText(BannedMap_06_MapName_Dropdown);

		match.PickedMap_07 = new PickedMap();
		match.PickedMap_07.MapName = GetDropdownText(PickedMap_07_MapName_Dropdown);
		match.PickedMap_07.BluePlayer.PlayerScore = GetDropdownTextAsInt(PickedMap_07_BluePlayerScore_Dropdown);
		match.PickedMap_07.RedPlayer.PlayerScore = GetDropdownTextAsInt(PickedMap_07_RedPlayerScore_Dropdown);
		if (match.BluePlayer.IsCoin || isUnknownCoin)
			match.PickedMap_07.BluePlayer.BannedChampionName = GetDropdownText(PickedMap_07_BluePlayerBan_Dropdown);
		if (match.RedPlayer.IsCoin || isUnknownCoin)
			match.PickedMap_07.RedPlayer.BannedChampionName = GetDropdownText(PickedMap_07_RedPlayerBan_Dropdown);
		match.PickedMap_07.BluePlayer.PickedChampionName = GetDropdownText(PickedMap_07_BluePlayerPick_Dropdown);
		match.PickedMap_07.RedPlayer.PickedChampionName = GetDropdownText(PickedMap_07_RedPlayerPick_Dropdown);

		//=== Вычисление пикеров карт
		if (isUnknownCoin)
		{
			match.BannedMap_01.Picker =
			match.BannedMap_02.Picker =
			match.PickedMap_03.Picker =
			match.PickedMap_04.Picker =
			match.BannedMap_05.Picker =
			match.BannedMap_06.Picker =
			match.PickedMap_07.Picker = PlayerSide.Unknown;
		}
		else
		{
			match.BannedMap_01.Picker = match.BluePlayer.IsCoin ? PlayerSide.Blue : PlayerSide.Red;
			match.BannedMap_02.Picker = match.BluePlayer.IsCoin ? PlayerSide.Red : PlayerSide.Blue;
			match.PickedMap_03.Picker = match.BluePlayer.IsCoin ? PlayerSide.Blue : PlayerSide.Red;
			match.PickedMap_04.Picker = match.BluePlayer.IsCoin ? PlayerSide.Red : PlayerSide.Blue;
			match.BannedMap_05.Picker = match.BluePlayer.IsCoin ? PlayerSide.Blue : PlayerSide.Red;
			match.BannedMap_06.Picker = match.BluePlayer.IsCoin ? PlayerSide.Red : PlayerSide.Blue;
			match.PickedMap_07.Picker = match.BluePlayer.IsCoin ? PlayerSide.Blue : PlayerSide.Red;
		}

		//=== Вычисление победителя матча

		if (match.PickedMap_03.BluePlayer.PlayerScore > match.PickedMap_03.RedPlayer.PlayerScore)
		{
			match.BluePlayer.MapsScore += 1;
			match.PickedMap_03.BluePlayer.IsWin = true;
		}
		else
		{
			match.RedPlayer.MapsScore += 1;
			match.PickedMap_03.RedPlayer.IsWin = true;
		}

		if (match.PickedMap_04.BluePlayer.PlayerScore > match.PickedMap_04.RedPlayer.PlayerScore)
		{
			match.BluePlayer.MapsScore += 1;
			match.PickedMap_04.BluePlayer.IsWin = true;
		}
		else
		{
			match.RedPlayer.MapsScore += 1;
			match.PickedMap_04.RedPlayer.IsWin = true;
		}

		if (match.PickedMap_07.BluePlayer.PlayerScore > match.PickedMap_07.RedPlayer.PlayerScore)
		{
			match.BluePlayer.MapsScore += 1;
			match.PickedMap_07.BluePlayer.IsWin = true;
		}
		else
		{
			match.RedPlayer.MapsScore += 1;
			match.PickedMap_07.RedPlayer.IsWin = true;
		}

		if (match.BluePlayer.MapsScore > match.RedPlayer.MapsScore)
			match.BluePlayer.IsWin = true;
		else
			match.RedPlayer.IsWin = true;

		//=== Поиск этапа для добавления матча

		var stageText = GetDropdownText(Stage_Dropdown);
		var isKickoff = stageText == "Kickoff";

		if (isKickoff)
		{
			match.MatchIndex = _league.Kickoff.Matches.Count;

			_league.Kickoff.Matches.Add(match);
		}
		else
		{
			Stage _stage = null;

			if (stageText == "Stage 1")
				_stage = _league.Stage_01;
			else if (stageText == "Stage 2")
				_stage = _league.Stage_02;
			else if (stageText == "Stage 3")
				_stage = _league.Stage_03;

			//=== Поиск недели для добавления матча

			var weekText = GetDropdownText(Week_Dropdown);
			if (weekText.StartsWith("Week"))
			{
				var week = _stage.Weeks[Convert.ToInt32(weekText.Replace("Week ", "")) - 1];
				match.MatchIndex = week.Matches.Count;
				week.Matches.Add(match);
			}
			else if (weekText == "Final")
			{
				var final = _stage.Final;
				match.MatchIndex = final.Matches.Count;
				final.Matches.Add(match);
			}
		}
	}

	private string GetDropdownText(TMP_Dropdown _dropdown)
	{
		return _dropdown.options[_dropdown.value].text;
	}

	private int GetDropdownTextAsInt(TMP_Dropdown _dropdown)
	{
		return (int)ConvertTools.StringToFloat(_dropdown.options[_dropdown.value].text);
	}

}