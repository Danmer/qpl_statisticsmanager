﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityZaichegTools_All;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Text;

public class General : Singleton<General>
{
	public League League;

	private string leagueFileUri = "F:/Projects_Sources/MyProjects/QPL_StatisticsManager/Output/League.xml";
	private string reportDirectoryUri = "F:/Projects_Sources/MyProjects/QPL_StatisticsManager/Output/Reports/";

	public void UnityAwakeEvent()
	{
		if (FileSystemTools.FileExists(leagueFileUri))
			ReadStageFile();
		else
			League = new League();
	}

	public void ReadStageFile()
	{
		var text = FileSystemTools.ReadAllText(leagueFileUri);
		League = XMLTools.Deserialize<League>(text);
	}

	public void SaveLeagueFile()
	{
		SaveXML(League, leagueFileUri);
	}

	public void AddMatchToLeagueFile()
	{
		GUIManager.Instance.AddDataFromUI(League);
		GUIManager.Instance.FillValues();
		SaveLeagueFile();
	}

	public void CreateReport_01()
	{
		Report_01 Report = new Report_01();

		var matches = League.GetAllMatches();
		foreach (var match in matches)
		{
			foreach (var pickedMap in match.GetAllPickedMaps())
			{
				Report_01_Map foundedReportMap = null;
				foreach (var reportMap in Report.Maps)
					if (reportMap.MapName == pickedMap.MapName)
						foundedReportMap = reportMap;
				if (foundedReportMap == null)
				{
					foundedReportMap = new Report_01_Map();
					foundedReportMap.MapName = pickedMap.MapName;
					Report.Maps.Add(foundedReportMap);
				}

				Report_01_Champion foundedBlueReportChampion = null;
				foreach (var reportChampion in foundedReportMap.Champions)
					if (reportChampion.ChampionName == pickedMap.BluePlayer.PickedChampionName)
						foundedBlueReportChampion = reportChampion;
				if (foundedBlueReportChampion == null)
				{
					foundedBlueReportChampion = new Report_01_Champion();
					foundedBlueReportChampion.ChampionName = pickedMap.BluePlayer.PickedChampionName;
					foundedReportMap.Champions.Add(foundedBlueReportChampion);
				}
				Report_01_Champion foundedRedReportChampion = null;
				foreach (var reportChampion in foundedReportMap.Champions)
					if (reportChampion.ChampionName == pickedMap.RedPlayer.PickedChampionName)
						foundedRedReportChampion = reportChampion;
				if (foundedRedReportChampion == null)
				{
					foundedRedReportChampion = new Report_01_Champion();
					foundedRedReportChampion.ChampionName = pickedMap.RedPlayer.PickedChampionName;
					foundedReportMap.Champions.Add(foundedRedReportChampion);
				}

				foundedBlueReportChampion.PicksCount += 1;
				foundedRedReportChampion.PicksCount += 1;
				if (pickedMap.RedPlayer.IsWin)
				{
					foundedBlueReportChampion.WinsCount += 1;
					foundedRedReportChampion.LossesCount += 1;
				}
				else
				{
					foundedRedReportChampion.WinsCount += 1;
					foundedBlueReportChampion.LossesCount += 1;
				}


				foundedBlueReportChampion = null;
				foreach (var reportChampion in foundedReportMap.Champions)
					if (reportChampion.ChampionName == pickedMap.BluePlayer.BannedChampionName)
						foundedBlueReportChampion = reportChampion;
				if (foundedBlueReportChampion == null
					&& string.IsNullOrEmpty(pickedMap.BluePlayer.BannedChampionName) == false
					&& pickedMap.BluePlayer.BannedChampionName != "Unknown")
				{
					foundedBlueReportChampion = new Report_01_Champion();
					foundedBlueReportChampion.ChampionName = pickedMap.BluePlayer.BannedChampionName;
					foundedReportMap.Champions.Add(foundedBlueReportChampion);
				}

				foundedRedReportChampion = null;
				foreach (var reportChampion in foundedReportMap.Champions)
					if (reportChampion.ChampionName == pickedMap.RedPlayer.BannedChampionName)
						foundedRedReportChampion = reportChampion;
				if (foundedRedReportChampion == null
					&& string.IsNullOrEmpty(pickedMap.RedPlayer.BannedChampionName) == false
					&& pickedMap.RedPlayer.BannedChampionName != "Unknown")
				{
					foundedRedReportChampion = new Report_01_Champion();
					foundedRedReportChampion.ChampionName = pickedMap.RedPlayer.BannedChampionName;
					foundedReportMap.Champions.Add(foundedRedReportChampion);
				}

				if (foundedBlueReportChampion != null)
					foundedBlueReportChampion.BansCount += 1;
				if (foundedRedReportChampion != null)
					foundedRedReportChampion.BansCount += 1;
			}
		}

		foreach (var reportMap in Report.Maps)
			foreach (var reportChampion in reportMap.Champions)
				if (reportChampion.WinsCount > 0)
					reportChampion.WinRate = ((float)reportChampion.WinsCount / reportChampion.PicksCount).ToString("f2");
				else if (reportChampion.PicksCount == 0)
					reportChampion.WinRate = "unknown";
				else
					reportChampion.WinRate = "0";

		Report.Maps.Sort();
		foreach (var reportMap in Report.Maps)
			reportMap.Champions.Sort();

		SaveReport(Report, "01_Map-Champion");
	}

	public void CreateReport_02()
	{
		Report_02 Report = new Report_02();

		var matches = League.GetAllMatches();
		foreach (var match in matches)
		{
			Report_02_Player foundedReportPlayer = null;
			foreach (var reportPlayer in Report.Players)
				if (reportPlayer.PlayerName == match.BluePlayer.PlayerName)
					foundedReportPlayer = reportPlayer;
			if (foundedReportPlayer == null)
			{
				foundedReportPlayer = new Report_02_Player();
				foundedReportPlayer.PlayerName = match.BluePlayer.PlayerName;
				Report.Players.Add(foundedReportPlayer);
			}

			if (match.BluePlayer.IsCoin)
			{
				foundedReportPlayer.CoinsCount += 1;

				foreach (var pickedMap in match.GetAllPickedMaps())
				{
					if (pickedMap.BluePlayer.IsWin)
						foundedReportPlayer.MapWinsCount += 1;
					else
						foundedReportPlayer.MapLossesCount += 1;
				}

				if (match.BluePlayer.IsWin)
					foundedReportPlayer.MatchWinsCount += 1;
				else
					foundedReportPlayer.MatchLossesCount += 1;
			}

			foundedReportPlayer = null;
			foreach (var reportPlayer in Report.Players)
				if (reportPlayer.PlayerName == match.RedPlayer.PlayerName)
					foundedReportPlayer = reportPlayer;
			if (foundedReportPlayer == null)
			{
				foundedReportPlayer = new Report_02_Player();
				foundedReportPlayer.PlayerName = match.RedPlayer.PlayerName;
				Report.Players.Add(foundedReportPlayer);
			}

			if (match.RedPlayer.IsCoin)
			{
				foundedReportPlayer.CoinsCount += 1;

				foreach (var pickedMap in match.GetAllPickedMaps())
				{
					if (pickedMap.RedPlayer.IsWin)
						foundedReportPlayer.MapWinsCount += 1;
					else
						foundedReportPlayer.MapLossesCount += 1;
				}

				if (match.RedPlayer.IsWin)
					foundedReportPlayer.MatchWinsCount += 1;
				else
					foundedReportPlayer.MatchLossesCount += 1;
			}
		}

		Report.Players.Sort();

		SaveReport(Report, "02_Player-Coin");
	}

	public void CreateReport_03()
	{
		Report_03 Report = new Report_03();

		var matches = League.GetAllMatches();
		foreach (var match in matches)
		{
			Report_03_Player foundedReportPlayer = null;
			foreach (var reportPlayer in Report.Players)
				if (reportPlayer.PlayerName == match.BluePlayer.PlayerName)
					foundedReportPlayer = reportPlayer;

			if (foundedReportPlayer == null)
			{
				foundedReportPlayer = new Report_03_Player();
				foundedReportPlayer.PlayerName = match.BluePlayer.PlayerName;
				Report.Players.Add(foundedReportPlayer);
			}

			Report_03_Enemy foundedReportEnemy = null;
			foreach (var reportEnemy in foundedReportPlayer.Enemies)
				if (reportEnemy.EnemyName == match.RedPlayer.PlayerName)
					foundedReportEnemy = reportEnemy;

			if (foundedReportEnemy == null)
			{
				foundedReportEnemy = new Report_03_Enemy();
				foundedReportEnemy.EnemyName = match.RedPlayer.PlayerName;
				foundedReportPlayer.Enemies.Add(foundedReportEnemy);
			}

			foundedReportEnemy.MatchesCount += 1;
			if (match.BluePlayer.IsCoin)
				foundedReportEnemy.CoinsCount += 1;
			if (match.BluePlayer.IsWin)
			{
				foundedReportEnemy.WinsCount += 1;
			}
			else
			{
				foundedReportEnemy.LossesCount += 1;
			}

			//=========

			foundedReportPlayer = null;
			foreach (var reportPlayer in Report.Players)
				if (reportPlayer.PlayerName == match.RedPlayer.PlayerName)
					foundedReportPlayer = reportPlayer;

			if (foundedReportPlayer == null)
			{
				foundedReportPlayer = new Report_03_Player();
				foundedReportPlayer.PlayerName = match.RedPlayer.PlayerName;
				Report.Players.Add(foundedReportPlayer);
			}

			foundedReportEnemy = null;
			foreach (var reportEnemy in foundedReportPlayer.Enemies)
				if (reportEnemy.EnemyName == match.BluePlayer.PlayerName)
					foundedReportEnemy = reportEnemy;

			if (foundedReportEnemy == null)
			{
				foundedReportEnemy = new Report_03_Enemy();
				foundedReportEnemy.EnemyName = match.BluePlayer.PlayerName;
				foundedReportPlayer.Enemies.Add(foundedReportEnemy);
			}

			foundedReportEnemy.MatchesCount += 1;
			if (match.RedPlayer.IsCoin)
				foundedReportEnemy.CoinsCount += 1;
			if (match.RedPlayer.IsWin)
			{
				foundedReportEnemy.WinsCount += 1;
			}
			else
			{
				foundedReportEnemy.LossesCount += 1;
			}
		}

		Report.Players.Sort();
		foreach (var reportPlayer in Report.Players)
			reportPlayer.Enemies.Sort();

		SaveReport(Report, "03_Player-Enemy-Coin");
	}

	public void CreateReport_04()
	{
		Report_04 Report = new Report_04();

		foreach (var match in League.GetAllMatches())
		{
			Report_04_Player foundedBlueReportPlayer = null;
			foreach (var reportPlayer in Report.Players)
				if (reportPlayer.PlayerName == match.BluePlayer.PlayerName)
					foundedBlueReportPlayer = reportPlayer;
			if (foundedBlueReportPlayer == null)
			{
				foundedBlueReportPlayer = new Report_04_Player();
				foundedBlueReportPlayer.PlayerName = match.BluePlayer.PlayerName;
				Report.Players.Add(foundedBlueReportPlayer);
			}
			Report_04_Player foundedRedReportPlayer = null;
			foreach (var reportPlayer in Report.Players)
				if (reportPlayer.PlayerName == match.RedPlayer.PlayerName)
					foundedRedReportPlayer = reportPlayer;
			if (foundedRedReportPlayer == null)
			{
				foundedRedReportPlayer = new Report_04_Player();
				foundedRedReportPlayer.PlayerName = match.RedPlayer.PlayerName;
				Report.Players.Add(foundedRedReportPlayer);
			}

			foreach (var pickedMap in match.GetAllPickedMaps())
			{
				Report_04_Champion foundedBlueReportChampion = null;
				foreach (var reportChampion in foundedBlueReportPlayer.Champions)
					if (reportChampion.ChampionName == pickedMap.BluePlayer.PickedChampionName)
						foundedBlueReportChampion = reportChampion;
				if (foundedBlueReportChampion == null)
				{
					foundedBlueReportChampion = new Report_04_Champion();
					foundedBlueReportChampion.ChampionName = pickedMap.BluePlayer.PickedChampionName;
					foundedBlueReportPlayer.Champions.Add(foundedBlueReportChampion);
				}
				Report_04_Champion foundedRedReportChampion = null;
				foreach (var reportChampion in foundedRedReportPlayer.Champions)
					if (reportChampion.ChampionName == pickedMap.RedPlayer.PickedChampionName)
						foundedRedReportChampion = reportChampion;
				if (foundedRedReportChampion == null)
				{
					foundedRedReportChampion = new Report_04_Champion();
					foundedRedReportChampion.ChampionName = pickedMap.RedPlayer.PickedChampionName;
					foundedRedReportPlayer.Champions.Add(foundedRedReportChampion);
				}

				foundedBlueReportChampion.MapsCount += 1;
				foundedRedReportChampion.MapsCount += 1;
				if (pickedMap.BluePlayer.IsWin)
				{
					foundedBlueReportChampion.WinsCount += 1;
					foundedRedReportChampion.LossesCount += 1;
				}
				else if (pickedMap.RedPlayer.IsWin)
				{
					foundedRedReportChampion.WinsCount += 1;
					foundedBlueReportChampion.LossesCount += 1;
				}
			}
		}

		foreach (var reportPlayer in Report.Players)
			foreach (var reportChampion in reportPlayer.Champions)
				reportChampion.WinRate = ((float)reportChampion.WinsCount / reportChampion.MapsCount).ToString("f2");

		Report.Players.Sort();
		foreach (var reportPlayer in Report.Players)
			reportPlayer.Champions.Sort();

		SaveReport(Report, "04_Player-Champion");
	}

	public void CreateReport_05()
	{
		Report_05 Report = new Report_05();

		foreach (var match in League.GetAllMatches())
			foreach (var map in match.GetAllPickedMaps())
			{
				Report.Rows.Add(new Report_05_Row()
				{
					MapName = map.MapName,
					ChampionName = map.BluePlayer.PickedChampionName,
					PlayerName = match.BluePlayer.PlayerName,
					EnemyName = match.RedPlayer.PlayerName,
					VideoLink = match.VideoLink
				});

				Report.Rows.Add(new Report_05_Row()
				{
					MapName = map.MapName,
					ChampionName = map.RedPlayer.PickedChampionName,
					PlayerName = match.RedPlayer.PlayerName,
					EnemyName = match.BluePlayer.PlayerName,
					VideoLink = match.VideoLink
				});
			}

		Report.Rows.Sort();

		SaveReport(Report, "05_Map-Champion-Player-Enemy-Videolink");
	}

	public void CreateReport_06()
	{
		Report_06 Report = new Report_06();

		foreach (var match in League.GetAllMatches())
		{
			Report_06_Player foundedBlueReportPlayer = null;
			foreach (var reportPlayer in Report.Players)
				if (reportPlayer.PlayerName == match.BluePlayer.PlayerName)
					foundedBlueReportPlayer = reportPlayer;
			if (foundedBlueReportPlayer == null)
			{
				foundedBlueReportPlayer = new Report_06_Player();
				foundedBlueReportPlayer.PlayerName = match.BluePlayer.PlayerName;
				Report.Players.Add(foundedBlueReportPlayer);
			}
			Report_06_Player foundedRedReportPlayer = null;
			foreach (var reportPlayer in Report.Players)
				if (reportPlayer.PlayerName == match.RedPlayer.PlayerName)
					foundedRedReportPlayer = reportPlayer;
			if (foundedRedReportPlayer == null)
			{
				foundedRedReportPlayer = new Report_06_Player();
				foundedRedReportPlayer.PlayerName = match.RedPlayer.PlayerName;
				Report.Players.Add(foundedRedReportPlayer);
			}

			foreach (var pickedMap in match.GetAllPickedMaps())
			{
				Report_06_Map foundedBlueReportMap = null;
				foreach (var reportMap in foundedBlueReportPlayer.Maps)
					if (reportMap.MapName == pickedMap.MapName)
						foundedBlueReportMap = reportMap;
				if (foundedBlueReportMap == null)
				{
					foundedBlueReportMap = new Report_06_Map();
					foundedBlueReportMap.MapName = pickedMap.MapName;
					foundedBlueReportPlayer.Maps.Add(foundedBlueReportMap);
				}
				Report_06_Map foundedRedReportMap = null;
				foreach (var reportMap in foundedRedReportPlayer.Maps)
					if (reportMap.MapName == pickedMap.MapName)
						foundedRedReportMap = reportMap;
				if (foundedRedReportMap == null)
				{
					foundedRedReportMap = new Report_06_Map();
					foundedRedReportMap.MapName = pickedMap.MapName;
					foundedRedReportPlayer.Maps.Add(foundedRedReportMap);
				}

				foundedBlueReportMap.MapsCount += 1;
				foundedRedReportMap.MapsCount += 1;
				if (pickedMap.BluePlayer.IsWin)
				{
					foundedBlueReportMap.WinsCount += 1;
					foundedRedReportMap.LossesCount += 1;
				}
				else if (pickedMap.RedPlayer.IsWin)
				{
					foundedRedReportMap.WinsCount += 1;
					foundedBlueReportMap.LossesCount += 1;
				}
			}
		}

		foreach (var reportPlayer in Report.Players)
			foreach (var reportMap in reportPlayer.Maps)
				reportMap.WinRate = ((float)reportMap.WinsCount / reportMap.MapsCount).ToString("f2");

		Report.Players.Sort();
		foreach (var reportPlayer in Report.Players)
			reportPlayer.Maps.Sort();

		SaveReport(Report, "06");
	}

	public void SaveReport<T>(T _obj, string _reportName)
	{
		SaveXML(_obj, reportDirectoryUri + _reportName + ".xml");
	}

	public void SaveXML<T>(T _obj, string _fileUri)
	{
		var xmlWriterSettings = new XmlWriterSettings() { Indent = true, Encoding = new UTF8Encoding(false) };
		var text = "";
		XmlSerializer xmlSerializer = new XmlSerializer(_obj.GetType());
		using (var stringWriter = new StringWriter())
		{
			using (XmlWriter writer = XmlWriter.Create(stringWriter, xmlWriterSettings))
			{
				try
				{
					xmlSerializer.Serialize(writer, _obj);
					//Debug.Log("Сериализация произведена успешна");
				}
				catch (SerializationException e)
				{
					Debug.Log("Ошибка сериализации. Причина: " + e.Message);
					throw;
				}

				text = stringWriter.ToString().Replace("utf-16", "utf-8");
			}
		}

		FileSystemTools.WriteAllText(_fileUri, text);
	}
}