﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityZaichegTools_All;

public class UnityEventManager : Singleton<UnityEventManager>
{
	private void OnEnable()
	{
		DataContainer.Instance.UnityAwakeEvent();
		General.Instance.UnityAwakeEvent();
		GUIManager.Instance.UnityOnEnableEvent();
	}
}