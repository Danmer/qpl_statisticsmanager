﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerSide
{
	Unknown,
	Blue,
	Red,
}