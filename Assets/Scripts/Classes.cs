﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class League
{
	public Kickoff Kickoff = new Kickoff();
	public Stage Stage_01 = new Stage(0);
	public Stage Stage_02 = new Stage(1);
	public Stage Stage_03 = new Stage(2);

	public List<Match> GetAllMatches()
	{
		var result = new List<Match>();

		var stages = new List<Stage>() { Stage_01, Stage_02, Stage_03 };
		result.AddRange(Kickoff.Matches);
		foreach (var stage in stages)
			foreach (var week in stage.Weeks)
				result.AddRange(week.Matches);

		return result;
	}
}

[Serializable]
public class Kickoff
{
	public List<Match> Matches = new List<Match>();
}

[Serializable]
public class Stage
{
	public int StageIndex;

	public List<Week> Weeks = new List<Week>();
	public Final Final = new Final();

	public Stage()
	{
	}

	public Stage(int _stageIndex)
	{
		StageIndex = _stageIndex;

		for (var i = 0; i < 10; i++)
			Weeks.Add(new Week(i));
	}
}

[Serializable]
public class Week
{
	public int WeekIndex;
	public List<Match> Matches = new List<Match>();

	public Week()
	{
	}

	public Week(int _weekIndex)
	{
		WeekIndex = _weekIndex;
	}
}

[Serializable]
public class Final
{
	public List<Match> Matches = new List<Match>();
}

[Serializable]
public class Match
{
	public int MatchIndex;

	public Player BluePlayer;
	public Player RedPlayer;

	public string VideoLink = "";

	public BannedMap BannedMap_01;
	public BannedMap BannedMap_02;
	public PickedMap PickedMap_03;
	public PickedMap PickedMap_04;
	public BannedMap BannedMap_05;
	public BannedMap BannedMap_06;
	public PickedMap PickedMap_07;

	public List<PickedMap> GetAllPickedMaps()
	{
		return new List<PickedMap>() { PickedMap_03, PickedMap_04, PickedMap_07 };
	}
}

[Serializable]
public class Player
{
	public string PlayerName = "";
	public bool IsCoin;
	public int MapsScore;
	public bool IsWin;
}

[Serializable]
public class BannedMap
{
	public string MapName = "";
	public PlayerSide Picker;
}

[Serializable]
public class PickedMap
{
	public string MapName = "";
	public PlayerSide Picker;

	public PlayerInMap BluePlayer = new PlayerInMap();
	public PlayerInMap RedPlayer = new PlayerInMap();
}

[Serializable]
public class PlayerInMap
{
	public string BannedChampionName = "";
	public string PickedChampionName = "";
	public int PlayerScore;
	public bool IsWin;
}

[Serializable]
public class Report_01
{
	public List<Report_01_Map> Maps = new List<Report_01_Map>();
}

[Serializable]
public class Report_01_Map : IComparable<Report_01_Map>
{
	public string MapName = "";
	public List<Report_01_Champion> Champions = new List<Report_01_Champion>();

	public int CompareTo(Report_01_Map _comparedItem)
	{
		return this.MapName.CompareTo(_comparedItem.MapName);
	}
}

[Serializable]
public class Report_01_Champion : IComparable<Report_01_Champion>
{
	public string ChampionName = "";
	public int PicksCount;
	public int BansCount;
	public int WinsCount;
	public int LossesCount;
	public string WinRate = "";

	public int CompareTo(Report_01_Champion _comparedItem)
	{
		return this.ChampionName.CompareTo(_comparedItem.ChampionName);
	}
}

[Serializable]
public class Report_02
{
	public List<Report_02_Player> Players = new List<Report_02_Player>();
}

[Serializable]
public class Report_02_Player : IComparable<Report_02_Player>
{
	public string PlayerName = "";
	public int CoinsCount;
	public int MatchWinsCount;
	public int MatchLossesCount;
	public int MapWinsCount;
	public int MapLossesCount;

	public int CompareTo(Report_02_Player _comparedItem)
	{
		return this.PlayerName.CompareTo(_comparedItem.PlayerName);
	}
}

[Serializable]
public class Report_03
{
	public List<Report_03_Player> Players = new List<Report_03_Player>();
}

[Serializable]
public class Report_03_Player : IComparable<Report_03_Player>
{
	public string PlayerName = "";
	public List<Report_03_Enemy> Enemies = new List<Report_03_Enemy>();

	public int CompareTo(Report_03_Player _comparedItem)
	{
		return this.PlayerName.CompareTo(_comparedItem.PlayerName);
	}
}

[Serializable]
public class Report_03_Enemy : IComparable<Report_03_Enemy>
{
	public string EnemyName = "";
	public int MatchesCount;
	public int WinsCount;
	public int LossesCount;
	public int CoinsCount;

	public int CompareTo(Report_03_Enemy _comparedItem)
	{
		return this.EnemyName.CompareTo(_comparedItem.EnemyName);
	}
}

[Serializable]
public class Report_04
{
	public List<Report_04_Player> Players = new List<Report_04_Player>();
}

[Serializable]
public class Report_04_Player : IComparable<Report_04_Player>
{
	public string PlayerName = "";
	public List<Report_04_Champion> Champions = new List<Report_04_Champion>();

	public int CompareTo(Report_04_Player _comparedItem)
	{
		return this.PlayerName.CompareTo(_comparedItem.PlayerName);
	}
}

[Serializable]
public class Report_04_Champion : IComparable<Report_04_Champion>
{
	public string ChampionName = "";
	public int MapsCount;
	public int WinsCount;
	public int LossesCount;
	public string WinRate = "";

	public int CompareTo(Report_04_Champion _comparedItem)
	{
		return this.ChampionName.CompareTo(_comparedItem.ChampionName);
	}
}

[Serializable]
public class Report_05
{
	public List<Report_05_Row> Rows = new List<Report_05_Row>();
}

[Serializable]
public class Report_05_Row : IComparable<Report_05_Row>
{
	public string MapName = "";
	public string ChampionName = "";
	public string PlayerName = "";
	public string EnemyName = "";
	public string VideoLink = "";

	public int CompareTo(Report_05_Row _comparedItem)
	{
		var result = this.MapName.CompareTo(_comparedItem.MapName);
		if (result == 0)
			result = this.ChampionName.CompareTo(_comparedItem.ChampionName);
		if (result == 0)
			result = this.PlayerName.CompareTo(_comparedItem.PlayerName);
		if (result == 0)
			result = this.EnemyName.CompareTo(_comparedItem.EnemyName);
		return result;
	}
}

[Serializable]
public class Report_06
{
	public List<Report_06_Player> Players = new List<Report_06_Player>();
}

[Serializable]
public class Report_06_Player : IComparable<Report_06_Player>
{
	public string PlayerName = "";
	public List<Report_06_Map> Maps = new List<Report_06_Map>();

	public int CompareTo(Report_06_Player _comparedItem)
	{
		return this.PlayerName.CompareTo(_comparedItem.PlayerName);
	}
}

[Serializable]
public class Report_06_Map : IComparable<Report_06_Map>
{
	public string MapName = "";
	public int MapsCount;
	public int WinsCount;
	public int LossesCount;
	public string WinRate = "";

	public int CompareTo(Report_06_Map _comparedItem)
	{
		return this.MapName.CompareTo(_comparedItem.MapName);
	}
}